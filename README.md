# auto-youtube-upload-script
A bash script for [youtube-upload](https://github.com/tokland/youtube-upload) (you need first to install youtube-upload and configure), to upload a lot of numberical video clips to YouTube.

These script reads all mp4's in ./record/, convert them into smaller files with fade in/out and creates thumbnails on his own. You can drop a thumb.png as layer, which comes over the thumbnails. This script merge the files together. After this a new dir will be created with all files (mp4 and jpg) which should be uploaded and a .sh file with all upload oneliners for youtube-upload. This will upload 4 files per day (because of youtube limitation) and will release one video day after day.
