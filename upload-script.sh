#!/bin/#!/usr/bin/env bash
# This bash script transcode automagical recorded files with OBS in record/ into smaller mp4 files with fade in and out in upload-files.
# The files would be renamed into same file names with counting numbers.
# In additional, it will create thumbnails which will be uploaded with youtube-upload.
# after the script is done, it will create a upload-$ folder with the videos, thumbnails, description and a upload script file.

# Create infrastructure

mkdir -p record
mkdir -p upload-files

touch last_upload_date.txt

# Create Discreption

echo '' > .source.tmp
echo '' > .desc.tmp

# Create YT infos

echo "Create YouTube Informations"
printf "Title:";
	read -r TITLE

printf "Category (Default Gaming):"
	read -r CATEGORY

printf "Playlist:"
	read -r PLAYLIST

printf "TAGS (seperate with commas. If Empty then native linux gaming):"
	read -r TAGS

# Create YT description file

printf "Developer Name:"
	read -r DEVS

printf "Linux Kernel Version:"
	read -r KERNEL

printf "CPU (Default i5 6500):"
	read -r CPU

printf "RAM (Default 16 GB DDR4):"
	read -r RAM

printf "Graphiccard (Default NVIDIA GTX 1060 6GB):"
	read -r GRAPHIC

printf "Driver Version (Default Nvidia proprietary driver):"
	read -r DRIVER

printf "Buy it on Steam:"
	read -r STEAM

printf "Buy it on HumbleBUndle:"
	read -r HB

printf "Buy it on GOG:"
	read -r GOG

printf "Buy it on Itch.io:"
	read -r ITCH

printf "Record Date:"
	read -r RECORD


if [ -z $CATEGORY ]; then
	CATEGORY="Gaming"
fi

if [ -z $TAGS ]; then
	TAGS="Games, Gaming, Linux, Walktrought, Arch Linux, Lets Play, Native"
fi

if [ -z $CPU ]; then
	CPU="Intel i5 6500"
fi

if [ -z $RAM ]; then
	RAM="16 GB DDR4"
fi

if [ -z $GRAPHIC ]; then
	GRAPHIC="NVIDIA GTX 1060 6GB"
fi

if [ -z $DRIVER ]; then
	DRIVER="Nvidia proprietary driver"
fi

if [ -z $STEAM ]; then
	STEAM="-"
fi

if [ -z $HB ]; then
        HB="-"
fi

if [ -z $GOG ]; then
        GOG="-"
fi

if [ -z $ITCH ]; then
        ITCH="-"
fi

echo "Title='\"$TITLE '" >>.source.tmp
echo "Kategorie='$CATEGORY'" >>.source.tmp
echo "Playlist='\"$PLAYLIST\"'" >>.source.tmp
echo "Tags='\"$TAGS\"'" >>.source.tmp

echo -e "$TITLE - Lets Play on Arch Linux\n\nEntiwckler: $DEVS\n\nAufgenommen mit: OBS Studio\nBetriebssystem: Arch Linux\nKernel: $KERNEL\n\nHardware\nCPU: $CPU\nRAM: $RAM\nGrafikkarte: $GRAPHIC\nTreiber: $DRIVER\n\nBuy it on Steam: $STEAM\nBuy it on Humblebundle: $HB\nBuy it on GOG: $GOG\nBuy it on Itch.io: $ITCH\n\nRecord date: $RECORD" > .desc.tmp


# Rename recorded file names

printf "Game Name?: "
	read -r FILES

if [ -z $FILES ]; then
	FILES=default-name
fi

cd record

a=1

for i in *.mp4; do
	new=$(printf "$FILES#%d.mp4" "$a")
	mv -i -- "$i" "$new"
	let a=a+1
done

# create thumbnails

for i in *.mp4; do
 name=`echo "$i" | cut -d'.' -f1`
 echo "$name"
 ffmpeg -y -i "$i" -vframes 1 -an -filter:v scale=1920:-1 -ss 30 ../upload-files/"${name}.jpg"
done

# modify thumbnail with thumb.png

for i in ../upload-files/*.jpg; do
 composite -gravity center thumb.png $i ../upload-files/$i
done

# Transcode the files into smaller ones

for i in *.mp4;
 do name=`echo "$i" | cut -d'.' -f1`
 echo "$name"
 ffmpeg -y \
 -i "$i" \
 -sseof -1 \
 -copyts \
 -hwaccel_device 0 \
 -hwaccel_output_format cuda \
 -hwaccel cuvid \
 -c:v h264_cuvid \
 -i "$i" \
 -b:v 5M \
 -minrate 5M \
 -maxrate 10M \
 -bufsize 10M \
 -profile:v high \
 -pixel_format cuda \
 -filter_complex \
 "[1]hwdownload,format=nv12,fade=out:0:30[t];\
 [0][t]overlay,fade=in:0:30[v];\
 anullsrc,atrim=0:2[at];[0][at]acrossfade=d=1,afade=d=1[a]" \
 -map "[v]" -map "[a]" \
 -vcodec h264_nvenc \
 ../upload-files/"${name}.mp4"
done

cd ..
# Modify Thumbnails With Numbers and Filename

cd upload-files

for i in *.jpg; do
  convert "$i" \
  -pointsize 240 \
  -gravity south -stroke '#000C' -strokewidth 10 \
  -annotate - "#$(echo $i| sed -e 's/^.*#//g' -e 's/.jpg//g')" \
  -stroke none -fill white \
  -annotate - "#$(echo $i| sed -e 's/^.*#//g' -e 's/.jpg//g')" \
  "$i";
done

cd ..
# Create Upload script

source .source.tmp

# Don't edit anything here
Path='upload-files/*.mp4'
Episoden=$(ls $Path | wc -l)
DayCount='0'
StartCount='1'
StopCount=$(expr $StartCount + $Episoden )
Sichtbarkeit='private'
Release='12:00:00.000+02:00'
File=$(ls $Path| sed -e 's/#.*//g' -e 's/upload-files\///g'| uniq)
StartDatum=$(cat last_upload_date.txt)

# simple loop script. This part generate a upload.sh for every clip you want to upload to YouTube,
# then it run the second script and after the upload the scond script will be deleted.
echo    "#!/bin/bash" > upload-files/upload-$(echo $Playlist|sed -e 's/\"//g' -e 's/ /_/g').sh

while [  $StartCount -lt $StopCount  ]; do
        echo youtube-upload \
          --thumbnail=\"$File#$StartCount.jpg\" \
          --title $Title\#$StartCount"\"" \
          --category=$Kategorie \
          --playlist=""$Playlist"" \
          --privacy=$Sichtbarkeit \
          --publish-at=$(date +%Y-%m-%d -d "$StartDatum + $DayCount day")T$Release \
          --tags=""$Tags"" \
          --description-file=desc-$(echo $Playlist|sed -e 's/\"//g' -e 's/ /_/g').txt \
          \"$File#$StartCount.mp4\"
        let StartCount=StartCount+1
        let DayCount=DayCount+1
done >> upload-files/upload-$(echo $Playlist|sed -e 's/\"//g' -e 's/ /_/g').sh

sed -i -e 's/^youtube/sleep 21600;youtube/g' -e 's/$/;/g' upload-files/upload-$(echo $Playlist|sed -e 's/\"//g' -e 's/ /_/g').sh
date +%Y-%m-%d -d "$(cat last_upload_date.txt) + $(ls $Path| wc -l) day" > last_upload_date.txt
mv upload-files/ upload-$(echo $Playlist|sed -e 's/\"//g' -e 's/ /_/g')
mkdir upload-files
cp .desc.tmp upload-$(echo $Playlist|sed -e 's/\"//g' -e 's/ /_/g')/desc-$(echo $Playlist|sed -e 's/\"//g' -e 's/ /_/g').txt


rm .source.tmp
rm .desc.tmp